calorine
========

.. image:: https://joss.theoj.org/papers/10.21105/joss.06264/status.svg
   :target: https://doi.org/10.21105/joss.06264

**calorine** is a Python package for running and analyzing molecular dynamics (MD) simulations via `GPUMD <https://gpumd.org/>`_.
It also provides functionality for constructing and sampling neuroevolution potential (NEP) models via `GPUMD <https://gpumd.org/>`_.
The full documentation can be found at https://calorine.materialsmodeling.org/.
