.. _credits:
.. index:: Credits

.. |br| raw:: html

  <br/>


Credits
*******

:program:`calorine` is primarily developed at the `Department of Physics <https://www.chalmers.se/en/departments/physics/Pages/default.aspx>`_ of `Chalmers University of Technology <https://www.chalmers.se/>`_ in Gothenburg, Sweden with funding from the Knut och Alice Wallenbergs Foundation, the Swedish Research Council, the Swedish Foundation for Strategic Research, and the National Academic Infrastructure for Supercomputing in Sweden.
         
When using :program:`calorine` in your research please cite the following papers:

* *calorine: A Python package for constructing and sampling neuroevolution potential models* |br|
  Lindgren *et al.*  |br|
  Journal of Open Source Software **9(95)**, 6264 (2024) |br|
  `doi: 10.21105/joss.06264  <https://doi.org/10.21105/joss.06264>`_ |br|
* *GPUMD: A package for constructing accurate machine-learned potentials and performing highly efficient atomistic simulations* |br|
  Fan *et al.*  |br|
  Journal of Chemical Physics **157**, 114801 (2022) |br|
  `doi: 10.1063/5.0106617  <https://doi.org/10.1063/5.0106617>`_ |br|
