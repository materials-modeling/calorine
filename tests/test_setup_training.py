from os.path import exists
from os.path import join as join_path

import numpy as np
import pytest
from ase.build import bulk
from ase.calculators.emt import EMT
from ase.io import read

from calorine.nep import read_nepfile, setup_training


@pytest.fixture
def nep_parameters():
    return dict(version=3,
                type=[2, 'Au', 'Cu'],
                cutoff=[8, 4],
                n_max=[8, 6],
                l_max=[4, 0],
                lambda_1=0.1,
                lambda_2=0.2,
                lambda_e=1,
                lambda_f=5,
                lambda_v=0.2,
                neuron=50,
                generation=200000)


@pytest.fixture
def structures():
    structures = []
    for i, alat in enumerate(np.arange(3, 6, 0.1)):
        structure = bulk('AuCu', crystalstructure='rocksalt', a=alat)
        structure.calc = EMT()
        structure.info['index'] = i
        structures.append(structure)
    return structures


@pytest.fixture
def setup_parameters(request):
    if request.param == 'kfold':
        return dict(mode='kfold',
                    rootdir='test_dir',
                    seed=42,
                    n_splits=3,
                    overwrite=True,
                    train_fraction=None)
    elif request.param == 'bagging':
        return dict(mode='bagging',
                    rootdir='test_dir',
                    train_fraction=0.8,
                    seed=42,
                    n_splits=3,
                    overwrite=True)


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training(nep_parameters, structures, setup_parameters):
    setup_training(nep_parameters, structures, **setup_parameters)
    rootdir = setup_parameters['rootdir']
    n_splits = setup_parameters['n_splits']
    mode = setup_parameters['mode']
    train_fraction = setup_parameters['train_fraction']
    assert exists(rootdir), 'Root directory not created'

    path = join_path(rootdir, 'nepmodel_full')
    assert exists(path), 'nepmodel_full subdirectory not created'
    assert exists(join_path(path, 'train.xyz')), 'Training structures not written'
    assert exists(join_path(path, 'test.xyz')), 'Testing structures not written'
    assert exists(join_path(path, 'nep.in')), 'nep.in not written'

    train_structures = read(join_path(path, 'train.xyz'), ':')
    test_structures = read(join_path(path, 'test.xyz'), ':')
    assert len(train_structures) == len(structures), \
        'Incorrect number of training structures generated'
    assert len(test_structures) == 1, \
        'Incorrect number of test structures generated'

    all_test_structures = []
    for k in range(1, n_splits + 1):
        path = join_path(rootdir, f'nepmodel_split{k}')
        assert exists(path), f'nepmodel_split{k} subdirectory not created'
        assert exists(join_path(path, 'train.xyz')), 'Training structures not written'
        assert exists(join_path(path, 'test.xyz')), 'Testing structures not written'
        assert exists(join_path(path, 'nep.in')), 'nep.in not written'

        train_structures = read(join_path(path, 'train.xyz'), ':')
        test_structures = read(join_path(path, 'test.xyz'), ':')
        if mode == 'kfold':
            test_size = int((1/n_splits) * len(structures))
            train_size = len(structures) - test_size
            assert len(train_structures) == train_size, \
                'Incorrect number of training structures generated'
            assert len(test_structures) == test_size, \
                'Incorrect number of test structures generated'
            all_test_structures.extend(test_structures)
        elif mode == 'bagging':
            train_size = int(train_fraction * len(structures))
            assert len(train_structures) == train_size, \
                'Incorrect number of training structures generated'
            assert len(test_structures) == len(structures) - train_size, \
                'Incorrect number of test structures generated'

    # make sure that all structures have been in the test set exactly once if kfold is used
    if mode == 'kfold':
        assert len(all_test_structures) == len(structures)
        indices = [s.info['index'] for s in structures]
        test_indices = [ts.info['index'] for ts in all_test_structures]
        assert len(set(test_indices)) == len(structures)
        assert set(indices) == set(test_indices)


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training_existing_dir_no_overwrite(nep_parameters, structures, setup_parameters):
    setup_parameters['overwrite'] = False
    with pytest.raises(FileExistsError) as excinfo:
        setup_training(nep_parameters, structures, **setup_parameters)
    assert 'Output directory exists. Set overwrite=True in' \
        ' order to override this behavior.' in str(excinfo.value), \
        'Incorrect error message for existing directory with no overwrite'


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training_invalid_n_splits(nep_parameters, structures, setup_parameters):
    setup_parameters['n_splits'] = 31
    with pytest.raises(ValueError) as excinfo:
        setup_training(nep_parameters, structures, **setup_parameters)
    assert 'n_splits (31) must be positive and must not exceed' in str(excinfo.value), \
        'Incorrect error message for invalid value for n_splits'


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training_n_splits_is_none(nep_parameters, structures, setup_parameters):
    setup_parameters['n_splits'] = None
    setup_training(nep_parameters, structures, **setup_parameters)
    path = join_path(setup_parameters['rootdir'], 'nepmodel_full')
    train_structures = read(join_path(path, 'train.xyz'), ':')
    test_structures = read(join_path(path, 'test.xyz'), ':')
    assert len(train_structures) == len(structures), \
        'Incorrect number of training structures generated'
    assert len(test_structures) == 1, \
        'Incorrect number of test structures generated'


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training_nep_parameters(nep_parameters, structures, setup_parameters):
    setup_parameters['n_splits'] = None
    setup_training(nep_parameters, structures, **setup_parameters)
    path = join_path(setup_parameters['rootdir'], 'nepmodel_full')
    rec = read_nepfile(join_path(path, 'nep.in'))
    for key, val_ref in nep_parameters.items():
        assert key in rec, f'{key} missing from nep.in'
        val_read = rec[key]
        assert np.all(val_read == val_ref), f'{key} incorrectly written'


@pytest.mark.parametrize('setup_parameters', [('bagging')], indirect=['setup_parameters'])
def test_setup_training_invalid_train_fraction_bagging(nep_parameters, structures,
                                                       setup_parameters):
    setup_parameters['train_fraction'] = 1.5
    with pytest.raises(ValueError) as excinfo:
        setup_training(nep_parameters, structures, **setup_parameters)
    assert 'train_fraction (1.5) must be in (0,1]' in str(excinfo.value), \
        'Incorrect error message for train_fraction outside of interval'


@pytest.mark.parametrize('setup_parameters', [('kfold')], indirect=['setup_parameters'])
def test_setup_training_invalid_train_fraction_kfold(nep_parameters, structures,
                                                     setup_parameters):
    setup_parameters['train_fraction'] = 1.5
    with pytest.raises(ValueError) as excinfo:
        setup_training(nep_parameters, structures, **setup_parameters)
    assert 'train_fraction cannot be set when mode kfold is used' in str(excinfo.value), \
        'Incorrect error message for train_fraction set while kfold mode is used'


@pytest.mark.parametrize('setup_parameters', [('kfold')], indirect=['setup_parameters'])
def test_setup_training_unknown_mode(nep_parameters, structures, setup_parameters):
    setup_parameters['mode'] = 'some-string'
    with pytest.raises(ValueError) as excinfo:
        setup_training(nep_parameters, structures, **setup_parameters)
    assert 'Unknown value for mode: some-string.' in \
        str(excinfo.value), 'Incorrect error message for unknwon mode'


@pytest.mark.parametrize('setup_parameters', [('kfold'), ('bagging')],
                         indirect=['setup_parameters'])
def test_setup_training_with_enforced(nep_parameters, structures, setup_parameters):
    """Assert that the enforced structures appear in all the training sets"""
    enforced_structures = [0, 1, 2, 3]
    setup_training(nep_parameters, structures,
                   enforced_structures=enforced_structures,
                   **setup_parameters)

    rootdir = setup_parameters['rootdir']
    n_splits = setup_parameters['n_splits']
    model_directories = [f'nepmodel_split{k}' for k in range(1, n_splits + 1)]
    model_directories.append('nepmodel_full')
    for model_dir in model_directories:
        path = join_path(rootdir, 'nepmodel_full')
        train_structures = read(join_path(path, 'train.xyz'), ':')
        for enforced_index in enforced_structures:
            # Assert that the structure appears exactly once
            # in the training set
            assert np.sum([s.info['index'] == enforced_index for s in train_structures]) == 1
