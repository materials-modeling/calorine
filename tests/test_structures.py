import numpy as np
import pytest
from ase.build import bulk
from ase.calculators.emt import EMT
from calorine.tools import relax_structure


@pytest.fixture
def structure():
    structure = bulk('Ag', a=4, cubic=True)
    structure[0].position += np.array([0.03, 0.08, 0])
    structure.calc = EMT()
    return structure


@pytest.mark.parametrize('minimizer, constant_cell, constant_volume, cell', [
    (
        'bfgs',
        False,
        False,
        [[4.064817846339863, 0.0004966154377642341, 0],
         [0.000496651391710738, 4.065115169926571, 0],
         [0, 0, 4.064765951302494]],
    ),
    (
        'bfgs',
        False,
        True,
        [[3.999921447087724, 0.0004068485249964212, 0],
         [0.00040688329652413936, 4.0002049334200604, 0],
         [0, 0, 3.9998736688917043]],
    ),
    (
        'lbfgs',
        False,
        True,
        [[3.9999214470877296, 0.00040684852500098975, 0],
         [0.0004068832965259793, 4.000204933420067, 0],
         [0, 0, 3.9998736688917043]],
    ),
    (
        'fire',
        True,
        False,  # Volume is fixed if cell is fixed
        [[4.0, 0.0, 0.0],
         [0.0, 4.0, 0.0],
         [0.0, 0.0, 4.0]],
    ),
    (
        'bfgs-scipy',
        False,
        True,
        [[3.9999253413995617, 0.0003874847077434158, 0],
         [0.00038751543338112987, 4.00019454113135, 0],
         [0, 0, 3.9998801622295908]],
    ),
    (
        'gpmin',
        False,
        True,
        [[3.999633385473468, -0.0005638399141078857, 0],
         [-0.0005649388855051413, 4.0004367660470095, 0],
         [0, 0, 3.9999299693705743]],
    ),
])
def test_check_fmax(structure,
                    minimizer,
                    constant_cell,
                    constant_volume,
                    cell):
    """Tests that a structure is relaxed to a maximum force lower than the specified threshold."""
    pos_pre_relax = structure.get_positions()
    relax_structure(
        structure,
        fmax=0.02,
        steps=10000,
        minimizer=minimizer,
        constant_cell=constant_cell,
        constant_volume=constant_volume,
    )
    fmax_post_relax = np.max(np.linalg.norm(structure.get_forces(), axis=1))
    assert fmax_post_relax < 0.03
    assert not np.allclose(structure.positions, pos_pre_relax)  # Make sure that the atoms moved
    assert np.isclose(structure.get_volume(), np.linalg.det(cell), atol=1e-4)
    new_cell = structure.get_cell()
    assert np.allclose(new_cell, cell, atol=1e-4)


@pytest.mark.parametrize('minimizer, constant_cell, constant_volume, cell, pressure', [
    (
        'bfgs',
        False,
        False,
        [[4.051720004851706, 0.00046165225507055756, 0],
         [0.0004616875342265811, 4.052007691079297, 0],
         [0, 0, 4.051670189842917]],
        1
    ),
    (
        'bfgs',
        False,
        True,
        [[3.999921447087724, 0.0004068485249964212, 0],
         [0.00040688329652413936, 4.0002049334200604, 0],
         [0, 0, 3.9998736688917043]],
        1
    ),
    (
        'lbfgs',
        False,
        True,
        [[3.9999214470877296, 0.00040684852500098975, 0],
         [0.0004068832965259793, 4.000204933420067, 0],
         [0, 0, 3.9998736688917043]],
        1
    ),
    (
        'fire',
        True,
        False,  # Volume is fixed if cell is fixed
        [[4.0, 0.0, 0.0],
         [0.0, 4.0, 0.0],
         [0.0, 0.0, 4.0]],
        1
    ),
    (
        'bfgs-scipy',
        False,
        True,
        [[3.9999253413995617, 0.0003874847077434158, 0],
         [0.00038751543338112987, 4.00019454113135, 0],
         [0, 0, 3.9998801622295908]],
        1
    ),
    (
        'gpmin',
        False,
        True,
        [[3.999633385473468, -0.0005638399141078857, 0],
         [-0.0005649388855051413, 4.0004367660470095, 0],
         [0, 0, 3.9999299693705743]],
        1
    ),
])
def test_check_fmax_under_external_pressure(structure,
                                            minimizer,
                                            constant_cell,
                                            constant_volume,
                                            cell,
                                            pressure):
    """
    Apply a high external pressure, and test that forces are still lowered below the force
    threshold. Cells with constant volume shouldn't change appreciably, but the ones without
    should. Hence, the expected cell for BFGS is different compared to the previous test.
    """
    pos_pre_relax = structure.get_positions()
    relax_structure(
        structure,
        fmax=0.02,
        steps=10000,
        minimizer=minimizer,
        constant_cell=constant_cell,
        constant_volume=constant_volume,
        scalar_pressure=pressure
    )
    fmax_post_relax = np.max(np.linalg.norm(structure.get_forces(), axis=1))
    assert fmax_post_relax < 0.03
    assert not np.allclose(structure.positions, pos_pre_relax)  # Make sure that the atoms moved
    assert np.isclose(structure.get_volume(), np.linalg.det(cell), atol=1e-4)
    new_cell = structure.get_cell()
    assert np.allclose(new_cell, cell, atol=1e-4)


@pytest.mark.parametrize('with_calculator, minimizer, error',
                         [
                             (False, 'fire', 'Structure has no attached calculator object'),
                             (True, 'invalid', 'Unknown minimizer: invalid'),
                          ]
                         )
def test_no_calculator(structure, with_calculator, minimizer, error):
    if not with_calculator:
        structure.calc = None
    with pytest.raises(ValueError) as e:
        relax_structure(structure, minimizer=minimizer)
    assert error in str(e)
