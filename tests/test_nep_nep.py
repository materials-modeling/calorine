import numpy as np
import pytest
from ase import Atoms
from ase.io import read

from calorine.nep.model import read_model
from calorine.nep.nep import (
    _predict_dipole_batch,
    _set_dummy_energy_forces,
    get_descriptors,
    get_dipole,
    get_dipole_gradient,
    get_polarizability,
    get_polarizability_gradient,
    get_latent_space,
    get_potential_forces_and_virials,
)


@pytest.fixture
def PbTe():
    return Atoms(
        'TePb',
        positions=[(0, 0, 0), (0, 0.0, 1.1)],
        cell=([100, 0, 0], [0, 100, 0], [0, 0, 100]),
    )


@pytest.fixture
def PbTe_no_cell():
    return Atoms(
        'TePb',
        positions=[(0, 0, 0), (0, 0.0, 1.1)]
    )


@pytest.fixture
def C():
    return Atoms('C', positions=[(0, 0, 0)])


@pytest.fixture
def CC():
    return Atoms('CC', positions=[(0, 0, 0), (0, 0.0, 1.1)])


@pytest.fixture
def CO():
    return Atoms('CO', positions=[(0, 0, 0), (0, 0.0, 1.1)])


@pytest.fixture
def CON():
    return Atoms('CON', positions=[(0, 0, 0), (0, 0.0, 1.1), (0, 0.0, 2.2)])


@pytest.fixture
def BaZrO3():
    return read('tests/example_files/polarizability/BaZrO3.xyz')


@pytest.fixture
def H2O():
    return read('tests/example_files/polarizability/H2O.xyz')


@pytest.fixture
def nep3_dipole():
    return 'tests/nep_models/nep4_dipole_Christian.txt'


@pytest.fixture
def nep3_pol():
    return 'tests/nep_models/nep3_polarizability_BaZrO3.txt'


@pytest.fixture
def nep4_pol():
    return 'tests/nep_models/nep4_polarizability_water_tnep.txt'


@pytest.fixture
def nep3_PbTe():
    return 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'


def get_expected(path):
    """Load forces or virials from file"""
    return np.loadtxt(path)


def _load_nep_from_file(file: str) -> str:
    """Load a NEP model from file into a stringified version

    Parameters
    ----------
    file
        Path to nep.txt.

    Returns
    -------
    str
        Stringified NEP model.
    """
    with open(file, 'r') as f:
        model = f.read()
    return model


# --- get_descriptors ---
def test_get_descriptors_no_cell(PbTe_no_cell, nep3_PbTe):
    """Should get descriptors for atoms without a specified cell, and raise a warning."""
    with pytest.warns(UserWarning) as record:
        descriptors = get_descriptors(PbTe_no_cell, model_filename=nep3_PbTe)
    assert len(record) == 1
    assert (
        record[0].message.args[0] == 'Using default unit cell (cubic with side 100 Å).'
    )
    assert descriptors.shape == (2, 30)


def test_get_descriptors_NEP3(PbTe):
    """Case: NEP3 model supplied. Compares results to output from `nep_cpu`"""
    nep3 = 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'
    descriptors_PbTe = get_descriptors(PbTe, model_filename=nep3)
    expected_PbTe = np.loadtxt(
        'tests/example_output/nep3_v3.3.1_PbTe_Fan22_PbTe_2atom_descriptor.out'
    )
    assert np.allclose(descriptors_PbTe, expected_PbTe, atol=1e-12, rtol=0)


def test_get_descriptors_debug(PbTe, nep3_PbTe, capsys):
    """Check that the output from GPUMD is printed to stdout when in debug mode"""
    get_descriptors(PbTe, model_filename=nep3_PbTe, debug=True)
    captured = capsys.readouterr()
    assert 'Use the NEP3 potential with 2 atom types' in captured.out
    assert captured.err == ''


# --- get_potential_forces_and_virials ---
def test_get_potential_forces_and_virials_NEP3(PbTe):
    """Case: NEP3 model supplied. Compares results to output from `nep_cpu`"""
    nep3 = 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'
    energies, forces, virials = get_potential_forces_and_virials(
        PbTe, model_filename=nep3
    )

    expected_forces = get_expected(
        'tests/example_output/nep3_v3.3.1_PbTe_Fan22_PbTe_2atom_force.out'
    )
    expected_virials = get_expected(
        'tests/example_output/nep3_v3.3.1_PbTe_Fan22_PbTe_2atom_virial.out'
    )

    assert energies.shape == (2,)
    assert forces.shape == (2, 3)
    assert virials.shape == (2, 9)
    assert np.allclose(forces, expected_forces, atol=1e-12, rtol=0)
    assert np.allclose(virials, expected_virials, atol=1e-12, rtol=0)


def test_get_potential_forces_and_virials_NEP3_debug(PbTe):
    """Compares result with debug flag enabled."""
    nep3 = 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'
    energies, forces, virials = get_potential_forces_and_virials(
        PbTe, model_filename=nep3, debug=True
    )

    expected_forces = get_expected(
        'tests/example_output/nep3_v3.3.1_PbTe_Fan22_PbTe_2atom_force.out'
    )
    expected_virials = get_expected(
        'tests/example_output/nep3_v3.3.1_PbTe_Fan22_PbTe_2atom_virial.out'
    )

    assert energies.shape == (2,)
    assert forces.shape == (2, 3)
    assert virials.shape == (2, 9)
    assert np.allclose(forces, expected_forces, atol=1e-12, rtol=0)
    assert np.allclose(virials, expected_virials, atol=1e-12, rtol=0)


def test_get_potential_forces_and_virials_no_potential(PbTe):
    """Tries to get potentials, forces and virials without specifying potential"""
    with pytest.raises(ValueError) as e:
        get_potential_forces_and_virials(PbTe)
    assert 'Model is undefined' in str(e)


def test_get_potential_forces_and_virials_invalid_potential(PbTe):
    """Tries to get potential with a dipole potential"""
    with pytest.raises(ValueError) as e:
        get_potential_forces_and_virials(
            PbTe, model_filename='tests/nep_models/nep4_dipole_Christian.txt'
        )
    assert (
        'A NEP model trained for predicting energies and forces must be used.' in str(e)
    )


def test_get_potential_forces_and_virials_malformed_potential(tmp_path, PbTe):
    """Tries to get potential energy with a malformed potential"""
    p = tmp_path / 'nep.txt'
    p.write_text((
        'npe3_lmao\n'
        'cutoff 8 4\n'
        'n_max 4 4\n'
        'l_max 4 2\n'
        'ANN 30 0\n'
        ' 1.0'
    ))
    with pytest.raises(ValueError) as e:
        get_potential_forces_and_virials(PbTe, model_filename=str(p))
    assert 'Unknown field: npe3_lmao' in str(
        e
    )


# --- get_dipole ---
def test_get_dipole_NEP3(nep3_dipole):
    """Case: NEP3 model supplied. Compares results to output from DFT."""
    structure = read('tests/example_files/dipole/test.xyz')
    dft_dipole = structure.get_dipole_moment()
    dipole = get_dipole(structure, model_filename=nep3_dipole)
    delta = dipole - dft_dipole
    assert dipole.shape == (3,)
    assert np.allclose(
        [-0.07468218, -0.03891397, -0.11160894], delta, atol=1e-12, rtol=1e-5
    )


def test_get_dipole_no_potential(PbTe):
    """Tries to get dipole without specifying potential"""
    with pytest.raises(ValueError) as e:
        get_dipole(PbTe)
    assert 'Model is undefined' in str(e)


def test_get_dipole_invalid_potential(PbTe):
    """Tries to get dipole with a non-dipole potential"""
    with pytest.raises(ValueError) as e:
        get_dipole(PbTe, model_filename='tests/nep_models/nep4_PbTe.txt')
    assert 'A NEP model trained for predicting dipoles must be used.' in str(e)


def test_dipole_consistent_CPU_GPU(nep3_dipole):
    """
    Make sure that the NEP_CPU implementation yields the same results as
    predicting with the NEP executable
    """
    structure = read('tests/example_files/dipole/test.xyz')
    structure = _set_dummy_energy_forces(structure)

    dipole = get_dipole(structure, model_filename=nep3_dipole)
    nep_dipole = _predict_dipole_batch(
        structures=[structure],
        model_filename=nep3_dipole,
        nep_command='nep',
    ) * len(structure)
    delta = dipole - nep_dipole
    assert nep_dipole.shape == (1, 3)
    assert np.allclose(
        [7.88235307e-05, 5.73983347e-05, -5.04966092e-06], delta, atol=1e-12, rtol=1e-5
    )


# --- get_dipole_gradient ---
def test_get_dipole_gradient(nep3_dipole):
    """Dipole gradients are computed using finite differences"""
    structure = read('tests/example_files/dipole/test.xyz')
    N = len(structure)
    # Test python implementation
    gradient_forward_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='python',
        method='forward difference',
        charge=1.0,
    )

    gradient_central_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='python',
        method='central difference',
        charge=1.0,
    )

    assert gradient_forward_python.shape == (N, 3, 3)
    assert gradient_central_python.shape == (N, 3, 3)
    assert not np.allclose(
        gradient_central_python, gradient_forward_python, atol=1e-12, rtol=1e-6
    )
    assert not np.allclose(gradient_forward_python, 0, atol=1e-12, rtol=1e-6)

    # Test nep implementation
    gradient_forward_nep = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,  # Results seems to become unstable below 1e-2; rounding errors?
        backend='nep',
        method='forward difference',
        charge=1.0,
        nep_command='nep',
    )

    gradient_central_nep = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='nep',
        method='central difference',
        charge=1.0,
        nep_command='nep',
    )

    assert gradient_forward_nep.shape == (N, 3, 3)
    assert gradient_central_nep.shape == (N, 3, 3)
    assert np.allclose(
        gradient_forward_nep, gradient_forward_python, atol=1e-1, rtol=1e-6
    )
    assert np.allclose(
        gradient_central_nep, gradient_central_python, atol=1e-1, rtol=1e-6
    )

    # Test CPU implementation
    gradient_forward_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='c++',
        method='forward difference',
        charge=1.0,
    )

    gradient_central_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='c++',
        method='central difference',
        charge=1.0,
    )

    assert gradient_forward_cpp.shape == (N, 3, 3)
    assert gradient_central_cpp.shape == (N, 3, 3)
    assert np.allclose(
        gradient_forward_cpp, gradient_forward_python, atol=1e-12, rtol=1e-6
    )
    assert np.allclose(
        gradient_central_cpp, gradient_central_python, atol=1e-12, rtol=1e-6
    )


def test_get_dipole_gradient_second_order(nep3_dipole):
    """Compare second order central difference to first order"""
    structure = read('tests/example_files/dipole/test.xyz')
    N = len(structure)

    gradient_first_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='python',
        method='central difference',
        charge=1.0,
    )
    gradient_second_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='python',
        method='second order central difference',
        charge=1.0,
    )
    assert gradient_first_python.shape == (N, 3, 3)
    assert gradient_second_python.shape == (N, 3, 3)
    # Second order should give somewhat the same results as first order
    # I.e. on the same order.
    assert np.allclose(
        gradient_first_python, gradient_second_python, atol=1e-1, rtol=1e-1
    )

    gradient_first_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='c++',
        method='central difference',
        charge=1.0,
    )

    gradient_second_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=1e-2,
        backend='c++',
        method='second order central difference',
        charge=1.0,
    )

    assert gradient_first_cpp.shape == (N, 3, 3)
    assert gradient_second_cpp.shape == (N, 3, 3)
    # Second order should give somewhat the same results as first order
    # I.e. on the same order.
    assert np.allclose(gradient_first_cpp, gradient_second_cpp, atol=1e-1, rtol=1e-1)

    # Should be numerically exact with Python
    assert np.allclose(
        gradient_second_cpp, gradient_second_python, atol=1e-12, rtol=1e-6
    )


def test_get_dipole_gradient_numeric(nep3_dipole):
    """Compare gradient to manually computed, for a two atom system"""
    structure = read('tests/example_files/dipole/test.xyz')[:2]

    # Calculate expected dipole gradient
    # Correct dipoles by the permanent dipole
    # charge = 2.0
    # copy = structure.copy()
    # dipole = (
    #     get_dipole(copy, model_filename=nep3_dipole) + charge * copy.get_center_of_mass()
    # )

    # displacement = 0.01
    # positions = copy.get_positions()
    # positions[0, 0] += displacement  # move in x direction
    # copy.set_positions(positions)
    # dipole_forward = (
    #     get_dipole(copy, model_filename=nep3_dipole) + charge * copy.get_center_of_mass()
    # )

    # expected = (dipole_forward - dipole) / displacement
    # print(expected)
    expected = [733.95084217, 4.56472784, 16.75684465]
    gradient_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='python',
        method='forward difference',
        charge=2.0,
    )
    gradient = gradient_python[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-12, rtol=1e-6)

    gradient_nep = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='nep',
        method='forward difference',
        charge=2.0,
        nep_command='nep',
    )
    gradient = gradient_nep[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-6, rtol=1e-3)

    gradient_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='c++',
        method='forward difference',
        charge=2.0,
    )
    gradient = gradient_cpp[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-12, rtol=1e-6)


def test_get_dipole_gradient_numeric_without_correction(nep3_dipole):
    """Compare gradient to manually computed, for a two atom system"""
    structure = read('tests/example_files/dipole/test.xyz')[:2]

    # Calculate expected dipole gradient
    # copy = structure.copy()
    # dipole = get_dipole(copy, model_filename=nep3_dipole)
    # displacement = 0.01
    # positions = copy.get_positions()
    # positions[0, 0] += displacement  # move in x direction
    # copy.set_positions(positions)
    # dipole_forward = get_dipole(copy, model_filename=nep3_dipole)
    # expected = (dipole_forward - dipole) / displacement
    # print(expected)
    expected = [733.14383155, 4.56472784, 16.75684465]
    gradient_python = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='python',
        method='forward difference',
        charge=0.0,
    )
    gradient = gradient_python[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-12, rtol=1e-6)

    gradient_nep = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='nep',
        method='forward difference',
        charge=0.0,
        nep_command='nep',
    )
    gradient = gradient_nep[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-6, rtol=1e-3)

    gradient_cpp = get_dipole_gradient(
        structure,
        model_filename=nep3_dipole,
        displacement=0.01,
        backend='c++',
        method='forward difference',
        charge=0.0,
    )
    gradient = gradient_cpp[0, 0, :]
    assert np.allclose(expected, gradient, atol=1e-12, rtol=1e-6)


def test_get_dipole_gradient_nep_too_small_displacement(nep3_dipole):
    """
    NEP implementation seems susceptible to small displacements,
    probably because of rounding errors.
    """
    structure = read('tests/example_files/dipole/test.xyz')
    N = len(structure)

    with pytest.warns(UserWarning) as record:
        gradient_nep = get_dipole_gradient(
            structure,
            model_filename=nep3_dipole,
            displacement=1e-6,
            backend='nep',
            method='forward difference',
            nep_command='nep',
        )
    assert (
        record[0].message.args[0]
        == 'Dipole gradients with nep are unstable for displacements < 0.01 Å.'
    )
    assert gradient_nep.shape == (N, 3, 3)


def test_get_dipole_gradient_invalid_potential(PbTe):
    """Tries to get dipole gradient with a non-dipole potential"""
    with pytest.raises(ValueError) as e:
        get_dipole_gradient(
            PbTe,
            model_filename='tests/nep_models/nep4_PbTe.txt',
            displacement=0.01,
            method='lmao',
            backend='c#',
        )
    assert 'A NEP model trained for predicting dipoles must be used.' in str(e)


def test_get_dipole_gradient_no_potential(PbTe):
    """Tries to get dipole gradient without a potential"""
    with pytest.raises(ValueError) as e:
        get_dipole_gradient(
            PbTe,
            model_filename=None,
            displacement=0.01,
            method='lmao',
            backend='c#',
        )
    assert 'Model is undefined' in str(e)


def test_get_dipole_gradient_invalid_backend(PbTe):
    """Tries to get dipole gradient whilst specifying an invalid backend"""
    with pytest.raises(ValueError) as e:
        get_dipole_gradient(
            PbTe,
            model_filename='tests/nep_models/nep4_dipole_Christian.txt',
            displacement=0.01,
            method='lmao',
            backend='c#',
        )
    assert 'Invalid backend c#' in str(e)


@pytest.mark.parametrize('backend', ['nep', 'python', 'c++'])
def test_get_dipole_gradient_invalid_method(PbTe, backend):
    """Tries to get dipole gradient whilst specifying an invalid method"""
    print(backend)
    with pytest.raises(ValueError) as e:
        get_dipole_gradient(
            PbTe,
            model_filename='tests/nep_models/nep4_dipole_Christian.txt',
            displacement=0.01,
            method='lmao',
            backend=backend,
        )
    assert 'Invalid method lmao for calculating gradient' in str(e)


@pytest.mark.parametrize('backend', ['nep', 'python', 'c++'])
def test_get_dipole_gradient_invalid_displacement(backend, PbTe):
    """Tries to get dipole gradient with an invalid displacement"""
    with pytest.raises(ValueError) as e:
        get_dipole_gradient(
            PbTe,
            model_filename='tests/nep_models/nep4_dipole_Christian.txt',
            displacement=0,
            backend=backend,
        )
    assert 'Displacement must be > 0 Å' in str(e)


# --- get_polarizability ---
def test_get_polarizability_NEP3(nep3_pol, BaZrO3):
    """Case: NEP3 model supplied. Compares results to output from DFT."""

    pol = get_polarizability(BaZrO3, model_filename=nep3_pol)
    dft_pol = BaZrO3.info['pol'].reshape(3, 3)
    delta = pol - dft_pol

    assert pol.shape == (3, 3)
    assert np.allclose(delta.mean(), 0.0323862, atol=1e-12, rtol=1e-5)


def test_get_polarizability_no_potential(PbTe):
    """Tries to get dipole without specifying potential"""
    with pytest.raises(ValueError) as e:
        get_polarizability(PbTe)
    assert 'Model is undefined' in str(e)


def test_get_polarizability_invalid_potential(PbTe):
    """Tries to get polarizability with a non-dipole potential"""
    with pytest.raises(ValueError) as e:
        get_polarizability(PbTe, model_filename='tests/nep_models/nep4_PbTe.txt')
    assert 'A NEP model trained for predicting polarizability must be used.' in str(e)


# --- get_polarizability_gradient ---
@pytest.mark.parametrize(
        'component,tensor_size', [
            ('x', 1),
            ('y', 1),
            ('z', 1),
            (['x', 'y'], 2),
            (['x', 'y', 'z'], 3),
            ('full', 3)
        ])
@pytest.mark.parametrize('model_fixture,structure_fixture', [
    ('nep3_pol', 'BaZrO3'),
    ('nep4_pol', 'H2O')
])
def test_get_polarizability_gradient(request,
                                     model_fixture,
                                     structure_fixture,
                                     component,
                                     tensor_size):
    """
    Polarizability gradients are computed using finite differences.
    For computational efficiency we allow for only computing certain
    components, which we test here.
    """
    # Ugly hacky workaround, but it's the only way
    # to use fixtures in mark.parametrize:
    # https://engineeringfordatascience.com/posts/pytest_fixtures_with_parameterize/
    model = request.getfixturevalue(model_fixture)
    structure = request.getfixturevalue(structure_fixture)

    N = len(structure)
    # Test python implementation
    gradient = get_polarizability_gradient(
        structure,
        model_filename=model,
        displacement=1e-2,
        component=component
    )

    assert gradient.shape == (N, tensor_size, 3, 3)
    assert not np.allclose(gradient, 0, atol=1e-12, rtol=1e-6)
    # Check symmetry
    for atom_grad in gradient:
        assert atom_grad.shape == (tensor_size, 3, 3)
        for cartesian in atom_grad:
            assert cartesian.shape == (3, 3)
            assert np.allclose(cartesian, cartesian.T, atol=1e-12, rtol=1e-6)


@pytest.mark.parametrize('component,message',
                         [('ax', 'Invalid component ax'),
                          ('hej', 'Invalid component hej'),
                          ('u wot', 'Invalid component u wot'),
                          (['lmao', 'invalid'], 'Invalid component lmao'),
                          (['full'], 'Write ``component="full"`` to get all components')]
                         )
def test_get_polarizability_gradient_invalid_components(nep3_pol, BaZrO3, component, message):
    with pytest.raises(ValueError) as e:
        get_polarizability_gradient(
            BaZrO3,
            model_filename=nep3_pol,
            displacement=1e-2,
            component=component
        )
    assert message in str(e)


def _get_shifted_polarizability(model, structure, d, atom=0, cartesian=0):
    """
    Utility function for computing shifted polarizabilities.
    By default shift the first atom in the x direction by d.
    """
    copy = structure.copy()
    positions = copy.get_positions()
    positions[atom, cartesian] += d  # move in x direction
    copy.set_positions(positions)
    shifted_pol = (
        get_polarizability(copy, model_filename=model)
    )
    return shifted_pol


@pytest.mark.parametrize(
        'component,index', [
            ('x', [0]),
            ('y', [1]),
            ('z', [2]),
            (['x', 'y'], [0, 1]),
            (['x', 'y', 'z'], [0, 1, 2]),
            (['x', 'x', 'x'], [0, 0, 0]),
            ('full', [0, 1, 2])
        ])
def test_get_polarizability_gradient_numeric(nep4_pol, component, index):
    """Compare gradient to manually computed, for a two atom system"""
    structure = Atoms('OH', [[0, 0, 0], [0, 0, 1.5]])
    # Calculate expected polarizability gradient
    displacement = 0.01
    expected_full = []
    for i in range(len(structure)):
        for j in range(3):
            pol_forward = _get_shifted_polarizability(
                    nep4_pol,
                    structure,
                    displacement,
                    atom=i,
                    cartesian=j
            )
            pol_backward = _get_shifted_polarizability(
                    nep4_pol,
                    structure,
                    -displacement,
                    atom=i,
                    cartesian=j
            )
            expected_atom_i_cart_j = (pol_forward - pol_backward) / (2*displacement)
            expected_full.append(expected_atom_i_cart_j)
    expected_full = np.array(expected_full).reshape(2, 3, 3, 3)
    expected = expected_full[:, index, :]
    gradient = get_polarizability_gradient(
        structure,
        model_filename=nep4_pol,
        displacement=displacement,
        component=component
    )
    # Really small values are possible off relatively (so a bit high rtol),
    # but the absolute errors should be really small.
    # C++ uses second order central differences whilst this Python script
    # only uses first order, leading to a slight systematic discrepancy.
    assert np.allclose(expected, gradient, atol=1e-12, rtol=5e-3)


def test_get_polarizability_gradient_numeric_hardcoded(nep4_pol):
    """Compare gradient to manually computed, for a two atom system"""
    structure = Atoms('OH', [[0, 0, 0], [0, 0, 1.5]])

    # Coompute numerical value
    # pol = (
    #     get_polarizability(structure, model_filename=nep4_pol)
    # )

    # displacement = 0.01
    # pol_forward = _get_shifted_polarizability(displacement)
    # pol_backward = _get_shifted_polarizability(-displacement)
    # expected_full = (pol_forward - pol_backward) / (2*displacement)

    expected = [
        [-3.33066907e-14,  0.0000000000e0, 5.75558865e-1],
        [0.0000000000e0, -2.22044605e-14, 0.000000000e0],
        [5.75558865e-01,  0.0000000000e0, 0.000000000e0]
    ]
    gradient_cpp = get_polarizability_gradient(
        structure,
        model_filename=nep4_pol,
        displacement=0.01,
        component='full'
    )
    gradient = gradient_cpp[0, 0, :, :]
    # Really small values are possible off relatively (so a bit high rtol),
    # but the absolute errors should be really small.
    # C++ uses second order central differences whilst this Python script
    # only uses first order, leading to a slight systematic discrepancy.
    assert np.allclose(expected, gradient, atol=1e-12, rtol=1e-4)


def test_get_polarizability_gradient_invalid_potential(PbTe):
    """Tries to get polarizability gradient with a non-dipole potential"""
    with pytest.raises(ValueError) as e:
        get_polarizability_gradient(
            PbTe,
            model_filename='tests/nep_models/nep4_PbTe.txt',
            displacement=0.01,
        )
    assert 'A NEP model trained for predicting polarizability must be used.' in str(e)


def test_get_polarizability_gradient_no_potential(PbTe):
    """Tries to get polarizability gradient without a potential"""
    with pytest.raises(ValueError) as e:
        get_polarizability_gradient(
            PbTe,
            model_filename=None,
            displacement=0.01
        )
    assert 'Model is undefined' in str(e)


def test_get_polarizability_gradient_invalid_displacement(PbTe, nep4_pol):
    """Tries to get dipole gradient with an invalid displacement"""
    with pytest.raises(ValueError) as e:
        get_polarizability_gradient(
            PbTe,
            model_filename=nep4_pol,
            displacement=0,
        )
    assert 'Displacement must be > 0 Å' in str(e)


# --- get_latent_space ---
def test_get_latent_space_NEP3(PbTe):
    """Case: NEP3 model supplied. Returns a latent space with expected shape."""
    nep3 = 'tests/nep_models/nep3_v3.3.1_PbTe_Fan22.txt'
    latent = get_latent_space(PbTe, model_filename=nep3)
    reference_latent = latent_space_reference(PbTe, nep3)
    assert latent.shape == (2, 30)
    assert np.allclose(latent, reference_latent)


def test_get_latent_space_no_potential(PbTe):
    """Tries to get latent space without specifying potential"""
    with pytest.raises(ValueError) as e:
        get_latent_space(PbTe)
    assert 'Model is undefined' in str(e)


def latent_space_reference(structure: Atoms, model_filename: str) -> np.ndarray:
    """Reference function for computing the latent space representation of a structure"""
    potential = read_model(model_filename)
    # Network parameters
    w0 = potential.ann_parameters['all_species']['w0']
    b0 = potential.ann_parameters['all_species']['b0']
    w1 = potential.ann_parameters['all_species']['w1']
    d = get_descriptors(structure, model_filename=model_filename)
    z0 = w0 @ d.T
    a0 = np.tanh(z0 - b0)
    z1 = a0.T * w1

    return z1


# --- error handling in nepy ---
def test_setup_nepy_invalid_model_filename(PbTe):
    """Should raise an error on dummy filename"""
    with pytest.raises(ValueError) as e:
        get_latent_space(PbTe, 'lol.txt')
    assert 'lol.txt does not exist' in str(e)
